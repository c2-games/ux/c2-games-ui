# c2-games-ui

## Developing

Note: project developed using Node v16.14.0

Once you've cloned the project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Environment variables

You will need the following environment variables in a `.env` file at the project root:

```bash
# Vars for Keycloak auth
PUBLIC_KEYCLOAK_URL=https://auth.ncaecybergames.org
PUBLIC_KEYCLOAK_REALM=dev
PUBLIC_KEYCLOAK_CLIENT_ID=c2-games-ui

# GraphQL Endpoint
PUBLIC_GRAPHQL_URL=http://localhost:8080/v1/graphql

# Event Info
PUBLIC_DEFAULT_EVENT_GROUP_NAME="NCAE CyberGames 2025 Regionals"
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Updating the Rules

To update the rules markdown file, download the Google Doc as a docx, and run this pandoc command:

```bash
pandoc -s Rules.docx --wrap=none --reference-links -t markdown -o src/lib/assets/content/rules.md
```
