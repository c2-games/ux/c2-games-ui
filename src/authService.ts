import {
  C2GamesProfile,
  isAuthenticated,
  oidcClient,
  authToken,
  authTokenParsed as userStore
} from './stores/authStore';
import { goto } from '$app/navigation';
import Env from '$lib/Env';
import { Log, SigninSilentArgs, SignoutSilentArgs, User, UserManager } from 'oidc-client-ts';
import { get, type Writable } from 'svelte/store';

export class KeycloakClient extends UserManager {
  async init() {
    /**
     * Init should be called once, immediately after the client is created.
     * It should be called outside the constructor, so it can be awaited,
     * otherwise race conditions may occur. Ex, between init.signinCallback and UserManager.signinSilent
     */
    let user = null;

    try {
      if (window.location.hash.includes('state=')) {
        user = (await this.signinCallback(window.location.href)) || null;
      }
    } catch (e) {
      console.error('init failed');
      console.error(e);
    } finally {
      window.location.hash = ''; // reset url
      this.updateStores(user);
    }
  }

  async signinSilent(args?: SigninSilentArgs): Promise<User | null> {
    const user = await super.signinSilent(args);
    this.updateStores(user);
    return user;
  }

  async signoutSilent(args?: SignoutSilentArgs): Promise<void> {
    this.updateStores(null);
    await super.signoutSilent(args);
  }

  updateStores(user: User | null) {
    // This could cause pain the future. When a token is refreshed,
    // an Access Token is guaranteed but an ID Token is not.
    // As far as we can tell, user.profile is a representation of the ID token and not the Access Token.
    // At time of writing, this is not an issue as Keycloak always populates the ID Token
    // WITH all information needed for C2GamesProfile such as hasura roles,
    // but a configuration change could break this in the future.
    const profile = user?.profile as C2GamesProfile;
    authToken.set(user?.access_token || null);
    isAuthenticated.set(!!profile);
    if (profile) {
      userStore.set({
        id: profile.sub,
        ...profile
      });
    } else {
      userStore.set(null);
    }
  }

  async login() {
    await this.signinRedirect();
  }

  async logout() {
    // Get to an unauthenticated page first, then logout and clear the stores
    await goto('/');
    await this.signoutSilent();
  }
}

export type CreateClientArgs = {
  trySilentAuth?: boolean;
  oidcLogLevel?: Log;
  statusStore?: Writable<string>;
};

export async function createClient(args?: CreateClientArgs): Promise<KeycloakClient> {
  args?.statusStore?.set('[createClient] start');
  if (get(oidcClient)) {
    console.error('oidcClient already exists'); // this happens in the dev server during hot-reloads
    return get(oidcClient);
  }
  const client = new KeycloakClient({
    authority: `${Env.KeycloakUrl}/realms/${Env.KeycloakRealm}`,
    client_id: Env.KeycloakClientId,
    // setting redirect_uri to window.location isn't perfect because it's the url when the page first loaded,
    // but this is a SPA and the url changes as the user navigates around. As of time of writing, there
    // are no routes that a user can click requiring authentication except the login button, so this is fine.
    // We may need to find some was to dynamically update this value in the future.
    // We use origin + pathname to discard the hash, which is used by oidc-client-ts.
    redirect_uri: window.location.origin + window.location.pathname,
    silent_redirect_uri: window.origin + '/ssoRedirect',
    silentRequestTimeoutInSeconds: 4,
    response_type: 'code',
    scope: 'openid',

    response_mode: 'fragment',
    filterProtocolClaims: true
  });
  await client.init(); // perform init outside the constructor so it can be awaited
  oidcClient.set(client);

  if (args?.oidcLogLevel) {
    args?.statusStore?.set('[createClient] setup logging');
    Log.setLogger({
      debug: console.debug,
      info: console.info,
      warn: console.warn,
      error: console.error
    });
    Log.setLevel(args.oidcLogLevel);
  }

  if (args?.trySilentAuth) {
    args?.statusStore?.set('[createClient] try silent auth');
    try {
      await client.signinSilent();
    } catch (err) {
      console.error(err);
    }
  }

  return client;
}
