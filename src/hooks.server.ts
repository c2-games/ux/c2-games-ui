import { setSession } from '$houdini';
import { get } from 'svelte/store';
import { authToken } from '$stores/authStore';
import type { Handle } from '@sveltejs/kit';

export const handle: Handle = async ({ event, resolve }): Promise<Response> => {
  // todo I'm not sure if this step in necessary?
  // if (!get(user) && get(keycloakClient)) {
  //   await login(get(keycloakClient));
  // }

  // this should work, but doesn't yet
  const newKcToken = get(authToken);

  setSession(event, { token: newKcToken });

  // pass the event onto the default handle
  return resolve(event);
};
