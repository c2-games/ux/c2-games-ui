// Used to map some generated Houdini GraphQL types to TypeScript types

import type {
  InviteUser$result,
  TeamByPk$input,
  TeamByPk$result,
  UserByEmail$result,
  EventGroups$result
} from '$houdini';
import type { ChecklistItem } from '$lib/interfaces/event-registration';
import { GetInstitutions$result } from '$houdini';

// https://stackoverflow.com/a/43001581/2834742
export type Writeable<T> = { -readonly [P in keyof T]: T[P] };
export type DeepWriteable<T> = { -readonly [P in keyof T]: DeepWriteable<T[P]> };

export type UUID = TeamByPk$input['team_id'];
export type User = NonNullable<UserByEmail$result['c2games_user_by_email']>;
export type InvitedUser = {
  user: NonNullable<NonNullable<InviteUser$result['c2games_user_invite']>['user']>;
  expiration: NonNullable<InviteUser$result['c2games_user_invite']>['expiration'];
};

export type Team = NonNullable<TeamByPk$result['teams_by_pk']>;
export type Institution = GetInstitutions$result['institutions'][0];
export type EventGroup = NonNullable<EventGroups$result['event_groups'][0]>;
export type Event = NonNullable<EventGroup['events'][0]>;
export type EventGroupTeam = NonNullable<Event['teams']['team'][0]>;
export type EventRegistrationSetting = NonNullable<Event['event_registration_setting']>;

export type EventWithChecklist = EventGroup['events'][0] & {
  checklist?: ChecklistItem[];
};
export type EventGroupWithChecklists = EventGroup & {
  events: EventWithChecklist[];
};
