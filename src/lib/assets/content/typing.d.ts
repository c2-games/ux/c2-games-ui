declare module '*.md' {
  // "unknown" would be more detailed depends on how you structure frontmatter
  export const attributes: Record<string, unknown>;

  // When "Mode.TOC" is requested
  export const toc: { level: string; content: string }[];

  // When "Mode.HTML" is requested
  export const html: string;
  // Note: This style of export was causing houdini to throw this error
  // so, exporting each piece individually is the work around.
  //
  // ❌ Encountered error in src/lib/assets/content/typing.d.ts
  // Export 'attributes' is not defined. (13:11)
  //
  // Modify below per your usage
  // export { attributes, toc, html };
}
