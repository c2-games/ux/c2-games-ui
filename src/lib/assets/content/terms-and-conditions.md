---
title: '[]{#_bzm7aujw4coq .anchor}Terms and Conditions'
---

## Participant Information/Liability Form

In consideration for being allowed to participate in this Activity, on behalf of myself and my next of kin, heirs and representatives, **I release from all liability and promise not to sue** C2Games and all of said entities' employees, officers, directors, volunteers and agents (collectively "C2Games") from any and all claims, **including claims of negligence by C2Games**, resulting in any physical or psychological injury (including paralysis and death), illness, damages, or economic or emotional loss I may suffer because of my participation in this Activity, including travel to, from, and during the Activity.

I am voluntarily participating in this Activity. I am aware of the risks associated with traveling to/from and participating in this Activity, which include but are not limited to physical or psychological injury, pain, suffering, illness, disfigurement, temporary or permanent disability (including paralysis), economic or emotional loss, and/or death. I understand that these injuries or outcomes may arise from my own or other's actions, inaction, or negligence; conditions related to travel; or the condition of the Activity location(s). **Nonetheless, I assume all related risks, both known or unknown to me, of my participation in this Activity, including travel to, from and during the Activity.**

I agree to **hold C2Games harmless** from any and all claims, including attorney's fees or damage to my personal property that may occur as a result of my participation in this Activity, including travel to, from and during the Activity. If C2Games incurs any of these types of expenses, I agree to reimburse C2Games. If I need medical treatment, I agree to be financially responsible for any costs incurred as a result of such treatment. I am aware and understand that I should carry my own health insurance.

**I understand the legal consequences of signing this document, including (a) releasing C2Games from all liability, (b) promising not to sue C2Games, (c) and assuming all risks of participating in this Activity, including travel to, from, and during the Activity.**

## Photo Release Form

I irrevocably grant to C2Games the absolute right:

- To take or cause to be taken videotape and photographs of me
- To copyright such videotape and photographs in the name of C2Games
- To use or authorize the use of the finished videotape and photographs and any reproductions thereof, severally or in conjunction with other videotape and photographs, in any medium, manner and form and for any purpose whatsoever, including without limitation illustration, promotion, advertising, trade, display, exhibition or use in any periodical, journal or other publication;

Furthermore, I assign to C2Games all right, title and interest I may have in the above-referenced videotape and photographs, including all finished copies, negatives and reproductions thereof and release and discharge C2Games including without limitation any claims for libel or alleged misrepresentation of me by virtue of alterations, or any use of such videotape and photographs authorized by C2Games.

Agreeing to these terms gives absolute right and permission to use participant's videotape(s) and photograph(s) in C2Games promotional materials and publicity efforts.
