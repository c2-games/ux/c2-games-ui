---
title: "[]{#_9j79bcku2rp6 .anchor}NCAE Cyber Games Code of Conduct"
---

# CONDUCT

NCAE Cyber Games is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, physical appearance, race, age or religion. We have developed this Code of Conduct in supplement to the in-development Code of Ethics as a guideline for expected behavior at our events. We do not tolerate harassment of conference participants in any form. Event participants violating these rules may be sanctioned or expelled from the conference at the discretion of the conference organizers.

Harassment includes, but is not limited to:

-   Verbal or text comments that reinforce social structures of domination: related to gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, age, religion

-   Sexual images in public spaces

-   Deliberate intimidation, stalking, or following

-   Harassing photography or recording

-   Sustained disruption of talks or other events

-   Inappropriate physical contact

-   Unwelcome sexual attention

-   Advocating for, or encouraging, any of the above behavior

# ENFORCEMENT

Participants asked to stop any harassing behavior are expected to comply immediately.

If a participant engages in harassing behavior, event organizers retain the right to take any actions to keep the event a welcoming environment for all participants. This includes warning the offender or expulsion from the event.

Event organizers may take action to redress anything designed to, or with the clear impact of, disrupting the event or making the environment hostile for any participants.

We expect participants to follow these rules at all event venues and event-related social activities. We think people should follow these rules outside event activities too!

# REPORTING

If someone makes you or anyone else feel unsafe or unwelcome, please report it as soon as possible. Event staff can be identified in Discord by Green usernames that designate them as staff, or use our online reporting tool. Harassment and other code of conduct violations reduce the value of our event for everyone. We want you to have a good experience at our event. Reporting undesirable behavior makes our event a better place.

You can make a report either personally or anonymously.

## Anonymous Report

You can make an anonymous report at [ncaecybergames.org/report](/report).

We can't follow up an anonymous report with you directly, but we will fully investigate it and take whatever action is necessary to prevent a recurrence. If the behavior continues, please make a follow-up report.

## Personal Report

You can make a personal report by:

-   Contacting a staff member, identified by STAFF Discord role (Green-colored name).

-   Optionally identifying yourself when submitting the anonymous report form at [ncaecybergames.org/report](/report).

When taking a personal report, our staff will never publicly identify you. They may involve other event staff to ensure your report is managed properly. Once safe, we will ask you to tell us about what happened. This can be upsetting, but we will handle it as respectfully as possible, and you can bring someone to support you. You won\'t be asked to confront anyone and we won\'t tell anyone who you are.
