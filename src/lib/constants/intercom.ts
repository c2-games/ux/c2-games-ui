export enum IntercomColorsMap {
  red = 'bg-red-600',
  blue = 'bg-blue-600',
  green = 'bg-green-600',
  yellow = 'bg-yellow-300',
  purple = 'bg-purple-300'
}
