import east_washington from '$lib/assets/images/teams/Eastern_Washington_Eagles_logo.svg.png';
import brigham from '$lib/assets/images/teams/BYU_Cougars_logo.svg.png';
import libertyU from '$lib/assets/images/teams/liberty_university.png';
import syracuse from '$lib/assets/images/teams/Syracuse_Orange_logo.svg.png';
import tulsa from '$lib/assets/images/teams/tulsa university logo.png';
import iitSeal from '$lib/assets/images/teams/illinois-tech-seal-only.svg';
import erauPrescott from '$lib/assets/images/teams/erau_prescott_alt.jpg';
import erauDaytona from '$lib/assets/images/teams/erau_daytona_alt.jpg';
import westFlorida from '$lib/assets/images/teams/uwf logo.png';
import universityFlorida from '$lib/assets/images/teams/university_florida_blue_UF_only.jpg';
import metroState from '$lib/assets/images/teams/metro state logo.png';
import caliStatePomona from '$lib/assets/images/teams/Cal_Poly_Pomona_Broncos_logo.svg.png';

export interface Team {
  name: string;
  imageUrl: string;
  href: string;
  imgStyle?: string;
}

export const winner: Team | null = {
  name: 'Eastern Washington University',
  imageUrl: east_washington,
  href: 'https://www.ewu.edu/'
};

export const finalists = {
  Northwest: [
    {
      name: 'Eastern Washington University',
      imageUrl: east_washington,
      href: 'https://www.ewu.edu/'
    },
    {
      name: 'Brigham Young University',
      imageUrl: brigham,
      href: 'https://www.byu.edu/'
    },
    {
      name: 'Prescott - Embry-Riddle Aeronautical University',
      imageUrl: erauPrescott,
      href: 'https://prescott.erau.edu/',
      imgStyle: 'border-radius: 50%;'
    }
  ] as Team[],
  Northeast: [
    {
      name: 'Liberty University',
      href: 'https://www.liberty.edu/',
      imageUrl: libertyU
    },
    {
      name: 'Syracuse University',
      href: 'https://www.syracuse.edu/',
      imageUrl: syracuse
    }
  ] as Team[],
  Southwest: [
    {
      name: 'University of Tulsa',
      href: 'https://utulsa.edu/',
      imgStyle: 'border-radius: 50%;',
      imageUrl: tulsa
    },
    {
      name: 'Cal Poly Pomona',
      href: 'https://www.cpp.edu/',
      imageUrl: caliStatePomona
    }
  ] as Team[],
  Midwest: [
    {
      name: 'Illinois Institute of Technology',
      href: 'https://www.iit.edu/',
      imageUrl: iitSeal
    },
    {
      name: 'Metro State University',
      href: 'https://www.metrostate.edu/',
      imgStyle: 'border-radius: 50%;',
      imageUrl: metroState
    }
  ] as Team[],
  Southeast: [
    {
      name: 'Daytona Beach - Embry-Riddle Aeronautical University',
      href: 'https://daytonabeach.erau.edu/',
      imageUrl: erauDaytona,
      imgStyle: 'border-radius: 50%;'
    },
    {
      name: 'University of West Florida',
      href: 'https://uwf.edu/',
      imageUrl: westFlorida,
      imgStyle: 'border-radius: 50%;'
    },
    {
      name: 'University of Florida',
      href: 'https://www.ufl.edu/',
      imgStyle: 'border-radius: 50%;',
      imageUrl: universityFlorida
    }
  ] as Team[]
};
