export const cards = [
  {
    background: "bg-[url('$lib/assets/images/home/collaborative.png')] bg-cover",
    header: 'Building a collaborative challenge for all',
    paragraphs: [
      'For college students, cyber competitions are an excellent way to gain hands-on experience and to explore the different roles available in the field. However, they can be intimidating for someone who has never tried a competition before. What skills do I need? How does scoring work? What if I’m not part of a hacking team?',
      'NCAE Cyber Games is designed to be a bridge for college students to learn how competitions run without the pressure of competing against established teams. The focus is on learning, with plenty of resources and support along the way!'
    ]
  },
  {
    background: "bg-[url('$lib/assets/images/home/expanding.png')] bg-cover",
    header: 'Expanding capacity across the country',
    paragraphs: [
      'To address our nation’s critical need for cybersecurity professionals, we must increase educational capacity while engaging students from historically underrepresented groups or who may not realize that there is a role for them in the cybersecurity industry.',
      'NCAE-C Cyber Games aims to increase the number of faculty and students participating in the cybersecurity community by looking beyond the traditional technical disciplines and providing the resources needed to initiate and provide a positive experience for newcomers to the field.'
    ]
  },
  {
    background: "bg-[url('$lib/assets/images/home/team_behind.png')] bg-cover",
    header: 'The team behind the games',
    paragraphs: [
      'NCAE Cyber Games represents a partnership between Mohawk Valley Community College located in Utica, New York, and Cyber Florida: The Florida Center for Cybersecurity hosted by the University of South Florida in Tampa, Florida. The team is composed of faculty members, cybersecurity professionals, and recent graduates who are experienced in participating in (and winning!) and designing cybersecurity competitions. The partnership combines Mohawk Valley’s success in designing and hosting cybersecurity competitions with Cyber Florida’s national program management and outreach expertise.'
    ]
  }
];
