import type { NavTab, NavLink } from '$lib/interfaces/navigation.interface';
import { faDiscord, faInstagram, faTwitch, faTwitter } from '@fortawesome/free-brands-svg-icons';
import Env from '$lib/Env';
import { discordInvite, getRegistrationUrl } from '$lib/constants/urls';
import type { AuthUser } from '$stores/authStore';

export const registerURL = `https://auth.ncaecybergames.org/realms/${Env.KeycloakRealm}/protocol/openid-connect/registrations?client_id=c2-games-ui&redirect_uri=https://www.ncaecybergames.org/user-dashboard&response_type=code`;

export function hasApiRole(token: AuthUser | undefined | null, role: string): boolean {
  return !!token?.['https://hasura.io/jwt/claims']?.['x-hasura-allowed-roles']?.includes(role);
}

export function shouldDisplay(
  link: NavLink | NavTab,
  isAuthenticated: boolean | undefined,
  user: AuthUser | null | undefined
) {
  // If auth is required, and we're not auth'd yet, don't display
  if (link.authRequired && !isAuthenticated) return false;
  // if a shouldDisplay function is not defined, then we always display
  if (!link.shouldDisplay) return true;
  // otherwise, check the return value of shouldDisplay
  return link.shouldDisplay(user);
}

const navTabs: Array<NavTab | NavLink> = [
  // REGISTER
  {
    label: 'Register',
    href: getRegistrationUrl(),
    shouldDisplay: (token) => !token // hide when authenticated
  } as NavLink,
  // SANDBOX
  {
    label: 'Sandbox',
    links: [
      { label: 'Sandbox Tutorial Videos', href: '/tutorials' },
      { label: 'How to Get Started', target: '_blank', href: 'https://ui.sandbox.ncaecybergames.org' },
      // { label: 'DEP Challenges', href: '/sandbox-challenges' },
      // todo get sandbox URL from config, hard-coded to PROD
      { label: 'Sandbox Challenges', target: '_blank', href: 'https://ui.sandbox.ncaecybergames.org/challenges' }
    ]
  } as NavTab,
  // ABOUT
  {
    label: 'About',
    links: [
      { label: 'The Competition', href: '/about' },
      { label: 'Schedule', href: '/schedule' },
      { label: 'Rules', href: '/rules' },
      { label: 'Code of Conduct', href: '/code-of-conduct' },
      { label: 'Terms and Conditions', href: '/terms-and-conditions' }
    ]
  } as NavTab,
  // MEDIA
  {
    label: 'Media',
    href: '/about/media'
  } as NavLink,
  // STUDENTS
  {
    label: 'Students',
    authRequired: true,
    links: [
      // This regex entry handles requiring authentication for the pages /teams/:ID and /teams/create
      { regex: /user-dashboard\/teams\/*/, authRequired: true, shouldDisplay: () => false },
      {
        label: 'Discover Teams',
        href: '/user-dashboard/teams/discover',
        authRequired: true
      },
      {
        label: 'Team Management',
        href: '/user-dashboard/teams',
        authRequired: true
      },
      {
        label: 'Results',
        href: '/user-dashboard/results',
        authRequired: true
      }
    ]
  } as NavTab,
  // STAFF
  {
    label: 'Staff',
    authRequired: true,
    // only display for users with staff role
    shouldDisplay: (token) => hasApiRole(token, 'staff'),
    links: [
      { label: 'Team Management', href: '/staff/team-management', authRequired: true },
      // todo update shouldDisplay for these
      {
        label: 'Check Deployment',
        href: '/staff/deployment/checks',
        authRequired: true,
        shouldDisplay: (token) => hasApiRole(token, 'event_admin')
      },
      {
        label: 'Awards',
        href: '/staff/awards',
        authRequired: true,
        shouldDisplay: (token) => hasApiRole(token, 'event_admin')
      }
    ]
  } as NavTab,
  // HELP
  {
    label: 'Help',
    links: [
      { label: 'FAQ', href: '/faq' },
      { label: 'Discord Setup', href: '/discord-setup' },
      { label: 'Contact Support', href: '/support' }
    ]
  } as NavTab,
  // CONNECT
  {
    label: 'Connect',
    links: [
      { label: 'Regional Contacts', href: '/regional-contacts' },
      { label: 'Contribute', href: '/contribute' },
      { label: 'Sponsorship', href: '/sponsorship' },
      { label: 'Discord', target: '_blank', href: discordInvite, icon: faDiscord },
      { label: 'Twitch', target: '_blank', href: 'https://www.twitch.tv/ncaecybergames', icon: faTwitch },
      { label: 'Twitter', target: '_blank', href: 'https://twitter.com/ncaecybergames', icon: faTwitter },
      { label: 'Instagram', target: '_blank', href: 'https://www.instagram.com/ncaecybergames/', icon: faInstagram }
    ]
  } as NavTab
];
export default navTabs;
