import { InvitedUser } from '$lib/graphql/types';

export type PendingUser = {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
};

export type UserInviteSuccessCallback = (u: InvitedUser) => Promise<void> | void;
