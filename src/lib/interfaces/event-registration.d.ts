export type ChecklistStatus = 'warning' | 'error';

export type ChecklistItem = {
  name: string;
  completed: boolean;
  status: ChecklistStatus;
  description: string;
};
