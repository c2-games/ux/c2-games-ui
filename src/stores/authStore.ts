import { UserInfoStore, type UserInfo$result } from '$houdini';
import { writable, type Writable } from 'svelte/store';
import type { UserProfile } from 'oidc-client-ts';
import type { KeycloakClient } from '../authService';

// represents the exact return from Keycloak
export type C2GamesProfile = UserProfile & {
  // These fields are known to be populated from Keycloak
  given_name: string;
  family_name: string;
  name: string;
  email: string;
  email_verified: boolean;
  preferred_username: string;
  'https://hasura.io/jwt/claims': {
    'x-hasura-default-role': string;
    'x-hasura-allowed-roles': string[];
    'x-hasura-user-email': string;
    'x-hasura-user-id': string;
  };

  exp: number;
};

// represents return from keycloak with additional fields remapped for ease of use
export type AuthUser = C2GamesProfile & {
  id: string; // sub
};

export const isAuthenticated: Writable<boolean | undefined> = writable(false);
export const oidcClient: Writable<KeycloakClient> = writable();
export const authToken: Writable<string | null> = writable(null);
export const authTokenParsed: Writable<AuthUser | null> = writable(null);
export const userInfo: Writable<UserInfo$result['c2games_user_by_pk'] | null> = writable(null);

oidcClient.subscribe((client) => console.debug('new oidc client', client));
authTokenParsed.subscribe((user) => console.debug('authTokenParsed', user));
authTokenParsed.subscribe(async (user) => {
  if (!user) return userInfo.set(null);
  const { data, errors } = await new UserInfoStore().fetch({ variables: { userId: user.id } });
  console.debug('userInfo', data, errors);

  if (errors?.length) {
    console.error('Failed to fetch userInfo', errors);
    return;
  }

  if (data?.c2games_user_by_pk) {
    userInfo.set(data.c2games_user_by_pk);
  }
});
