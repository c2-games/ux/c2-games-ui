import { UserTeamsSubStore, DiscoverTeamsSubStore, type UserTeamsSub$result } from '$houdini';
import { writable } from 'svelte/store';

// These subscription stores will start listening in +layout.svelte
export const userTeams = new UserTeamsSubStore();
export const discoverableTeams = new DiscoverTeamsSubStore();
export const activeTeams = writable<UserTeamsSub$result['teams']>([]);
export const archivedTeams = writable<UserTeamsSub$result['teams']>([]);
userTeams.subscribe((result) => {
  if (result.errors?.length) console.error('Failed to subscribe userTeams', result.errors);

  archivedTeams.set((result.data?.teams || []).filter((t) => t.events.filter((e) => e.event.ended).length > 0));
  activeTeams.set((result.data?.teams || []).filter((t) => t.events.every((e) => !e.event.ended)));
});
discoverableTeams.subscribe((data) => {
  if (data.errors?.length) console.error('Failed to subscribe discoverableTeams', data.errors);
});
