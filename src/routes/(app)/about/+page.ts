import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
  return { banner: { title: 'WELCOME TO YOUR FIRST CYBER COMPETITION' } };
};
