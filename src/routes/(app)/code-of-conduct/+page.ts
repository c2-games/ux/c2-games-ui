import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
  return { banner: { title: 'NCAE Cyber Games Code of Conduct' } };
};
