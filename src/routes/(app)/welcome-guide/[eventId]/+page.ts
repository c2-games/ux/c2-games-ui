type PageParameters = {
  params: { eventId: string };
};

export async function load({ params }: PageParameters) {
  return {
    eventId: params.eventId,
    banner: { title: 'Welcome Guide' }
  };
}
