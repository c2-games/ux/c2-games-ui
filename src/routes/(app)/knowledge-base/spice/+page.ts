import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
  return { banner: { title: 'SPICE Console Usage' } };
};
